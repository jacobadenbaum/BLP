import Base.getindex, Base.setindex!, Base.show

abstract type Model end

# Example Interface
# type ExampleModel <: Model
#     # Any model parameters get stored in a lookup table
#     parameters::Dict{Symbol, Any}
# 
#     # You could put any other fields here that you want if you need them
#     # for your specific implementation
#     #
#     #
#     #               (Your Fields Here!)
#     #
#     #
# end

# Indexing just looks up parameters
getindex(m::Model, key) = m.parameters[key]
getindex(m::Model, keys::AbstractArray) = [m[key] for key in keys]

function setindex!(m::Model, val, key)
    m.parameters[key] = val
    return m
end

# Print the model nice and pretty
function show(io::IO, x::Model)
    T = typeof(x)
    out = "$T("
    for key in keys(x.parameters)
        out *= ":$key=$(x[key])"
    end
    out *= ")"
    print(io, out)
end

