using DataFrames

########################################################################
#################### Loading and Cleaning Data #########################
########################################################################

# Load the data
df = readtable("data/cardata.txt", separator='\t')
for key in [:ac_standard, :firmid]
    df[key] = Float64.(df[key])
end

# Drop any duplicate observations
unique!(df)

# Compute in each market the share who did not purchase
df = by(df, :year) do x
    new = DataFrame()
    for name in names(x)
        new[name] = x[name]
    end
    new[:s0] = 1 - sum(new[:market_share])
    return new
end
df[:constant] = ones(size(df, 1))

########################################################################
#### Some quick checks about the data to confirm what I think I know: ##
########################################################################

# 1) Vehicle Name and Year uniquely identify observations in the data
# set

#"""
#Function to check whether or not a set of variables uniquely identifies
#the observations in a dataframe
#"""
#function check_ids(df, groupvars)
#    data = by(df, groupvars) do x
#        new = DataFrame()
#        for name in names(x)
#            if !(name in groupvars)
#                new[name] = x[name]
#            end
#        end
#        new[:nobs] = size(x,1)
#        return new end
#
#    if all(data[:nobs] .== 1)
#        println("Observations are Uniquely Identified by $groupvars")
#    else
#        # Count the number of duplicates
#        println("""
#            There are $(sum(data[:nobs].>1)) observations which are not
#            uniquely identified by $groupvars """)
#    end
#    return data
#end
## We should drop the bad observations until we understand them better.
#groupvars = [:vehicle_name, :year]
#df = check_ids(df, groupvars)
#println("Dropping bad Observations")
#df = df[df[:nobs].==1, :]

