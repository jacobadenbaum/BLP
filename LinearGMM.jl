type LinearGMM
    y       # Independent Variable
    Z       # Observables 
    X       # Instruments 
    n       # Number of observations
    vvcov   # Variance Covariance Function
    _δ̂      # Computed Parameters
    _ϵ̂      # Computed Residuals
    _Ŵ      # Most Recent Ŵ
    LinearGMM(y, Z, X, n, vvcov) = new(y, Z, X, n, vvcov)
end

function LinearGMM(y,Z,X; vvcov=white)
    
    # Check that we have the right number of observations
    n = length(y)
    @assert(n == size(Z, 1) == size(X,1), 
        "Incorrect Number of Observations")

    # Check that we have more instruments than endogenous variables
    @assert(size(X, 2) >= size(Z,2), 
        "Model is Not Identified")

    return LinearGMM(y, Z, X, n, vvcov)

end

function estimate!(m::LinearGMM, Ŵ)
    

    # Unpack Model
    y = m.y
    Z = m.Z
    X = m.X
    n = m.n

    Sxz = (1/n)*X'*Z
    Sxy = (1/n)*X'*y

    δ̂   = inv(Sxz'*Ŵ*Sxz)*Sxz'*Ŵ*Sxy
    ϵ̂   = y - Z*δ̂
    
    # Store Computed Values
    m._δ̂ = δ̂
    m._ϵ̂ = ϵ̂
    m._Ŵ = Ŵ
    
    return δ̂
end

res(m::LinearGMM) = m._ϵ̂
params(m::LinearGMM) = m._δ̂

function white(m::LinearGMM)
    # Unpack model
    n = m.n
    X = m.X
    k = size(X,2)
    
    # Get the residuals from the model
    ϵ̂ = res(m)
    
    Ŝ = zeros(k,k)
    for (i, ϵ̂i) in enumerate(ϵ̂)
        Ŝ += ϵ̂i^2*X[i,:]*X[i,:]'
    end

    return Ŝ/n
end

function avar(m::LinearGMM, Ŵ)
    
    # Unpack Model
    vvcov = m.vvcov
    y = m.y
    Z = m.Z
    X = m.X
    n = m.n
    
    # Compute Covariances
    Sxz = (1/n)*X'*Z
    Sxy = (1/n)*X'*y

    # Compute Variance of Moments (estimate)
    Ŝ = vvcov(m)
    
    # Compute inverse once
    B = inv(Sxz'*Ŵ*Sxz)

    return B*Sxz'*Ŵ*Ŝ*Ŵ*Sxz*B
end

# If you don't specify the Ŵ, use the most recent value
avar(m::LinearGMM) = avar(m, m._Ŵ)

# Compute the Efficient GMM Estimator -- Two Step
function estimate!(m::LinearGMM)
    # Unpack Model
    y = m.y
    Z = m.Z
    X = m.X
    n = m.n

    Sxz = (1/n)*X'*Z
    Sxx = (1/n)*X'*X
    Sxy = (1/n)*X'*y

    # Start with the Weighting Matrix inv(Sxx) (Two stage least squares)
    estimate!(m, inv(Sxx))

    # Compute the Implied Ŝ
    Ŝ = m.vvcov(m)

    # Re-estimate with Ŵ = inv(Ŝ)
    δ̂ = estimate!(m, inv(Ŝ))

    return δ̂
end

se(m::LinearGMM) = sqrt.(diag(avar(m))/m.n)
