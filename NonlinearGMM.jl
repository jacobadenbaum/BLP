using NLopt
using ForwardDiff

"""
```
NonlinearGMM(data; vvcov=sample_variance)
```
Interface to Nonlinear GMM:
  Need to pass it a data type that has the following methods
  implemented:

      1)  `moments(data, δ, W)`:
            
            returns an iterable that computes the
            moment condition applied to each observation.  

            Note that we must pass W to allow us to perform linear
            reductions of the parameter space within the moment
            function
      
      2)  `nparams(data)`:
      
            returns the number of parameters
      
      3)  `nmoments(data)`:
            
            returns the number of moment conditions
"""
type NonlinearGMM 
    data    # Model Data (Must have `moments` function implemented)
    vvcov   # Variance Covariance Function
    _δ̂      # Computed Parameters
    _ĝ      # Computed Moment Residuals
    _Ŵ      # Most Recent Ŵ
    NonlinearGMM(data, vvcov::Function) = new(data, vvcov)
end

function NonlinearGMM(data; vvcov=sample_variance)
    
    # Check that we have more instruments than endogenous variables
    @assert(nmoments(data) >= nparams(data),
        "Model is Not Identified")

    return NonlinearGMM(data, vvcov)

end

function estimate!(m::NonlinearGMM, Ŵ, δ0)
    function objective(δ, grad)
        g = mean(moments(m.data, δ))
        return sum(g'*Ŵ*g)
    end

    opt = Opt(:LN_COBYLA, Int(nparams(m.data)/2))
    lower_bounds!(opt, lower(m.data))
    upper_bounds!(opt, upper(m.data))
    xtol_rel!(opt, 1e-4)

    min_objective!(opt, objective)
    minf, δ̂, ret = optimize(opt, δ0)
    ĝ = collect(moments(m.data, δ̂))
    
    # Store Computed Values
    m._δ̂ = δ̂
    m._ϵ̂ = res(m.data, δ)
    m._Ŵ = Ŵ
    
    return δ̂
end

res(m::NonlinearGMM) = m._ĝ
params(m::NonlinearGMM) = m._δ̂

function sample_variance(m::NonlinearGMM)
    
    # Get the residuals from the model
    ϵ̂ = res(m)
    k = nmoments(m.data)
    n = length(ϵ̂)

    Ŝ = zeros(k,k)
    for (i, ϵi) in enumerate(ϵ̂)
        Ŝ += ϵi^2*Z[i,:]*Z[i,:]'
    end

    return Ŝ/n
end

function avar(m::NonlinearGMM, Ŵ)
    
    # Unpack Model
    vvcov = m.vvcov
    
    # Compute Variance of Moments (estimate)
    Ŝ = vvcov(m)
    
    # Compute Derivative of Moment Condition
    δ̂ = params(m)
    Ĝ = ForwardDiff.gradient(x->mean(moments(m.data, x)), δ̂)

    # Compute inverse once
    B = inv(Ĝ'*Ŵ*Ĝ)

    return B*Ĝ'*Ŵ*Ŝ*Ŵ*Ĝ*B
end

# If you don't specify the Ŵ, use the most recent value
avar(m::NonlinearGMM) = avar(m, m._Ŵ)

# Compute the Efficient GMM Estimator -- Two Step
function estimate!(m::NonlinearGMM, δ0)

    # Start with the Weighting Matrix inv(Sxx) (Two stage least squares)
    k = nmoments(m.data)
    l = nparams(m.data)
    estimate!(m, eye(k))

    # Compute the Implied Ŝ
    Ŝ = m.vvcov(m)

    # Re-estimate with Ŵ = inv(Ŝ)
    δ̂ = estimate!(m, inv(Ŝ), zeros(l))

    return δ̂
end

se(m::NonlinearGMM) = sqrt.(diag(avar(m))/m.n)
