module Tables

# Nice string formattting
using Formatting

# Import from base to extend
import Base.getindex, Base.setindex!, Base.push!

export TableCol, Table, tex, write_tex

########################################################################
#################### Table Column Type #################################
########################################################################

type TableCol
    header
    keys::Vector
    values::Vector
    fmt
end

function TableCol(header, keys, values; fmt="{:.2f}")
    return TableCol(header, keys[:], values[:], fmt)
end

function getindex(col::TableCol, x, backup="")
    # Returns the formatted value if it exists
    idx = findfirst(col.keys, x)
    
    if idx > 0
        # Get the value
        val = format(col.fmt, col.values[idx])
    else
        val = backup
    end

    # Get the column length
    l = max(length(String(col.header)), length(val)) 
    return format("{:<$l}", val)
end

function setindex!(col::TableCol, value, key)
    idx = findfirst(col.keys, key)
    if idx == 0
        push!(col.keys, key)
        push!(col.values, value)
    else 
        col.values[idx] = value
    end

    return col
end

########################################################################
#################### Full Table Type ###################################
########################################################################

type Table
    Columns::Vector
    RowHeader::Vector
    ColHeader::Vector
    Table() = new([],[],[])
end

function Table(Columns::Vector, RowHeader::Vector, ColHeader::Vector)
    return Table(Columns, RowHeader, ColHeader)
end

function Table(args...; kwargs...)
    t = Table() 
    for col in args
        push!(t, col)
    end
    return t
end

function push!(t::Table, newcol::TableCol)
    # Add to the list of columns
    push!(t.Columns, newcol)

    # Update the Row Headers
    for key in newcol.keys
        if !(key in t.RowHeader)
            push!(t.RowHeader, key)
        end
    end

    # Update the Column Headers
    push!(t.ColHeader, newcol.header)

    return t
end

function getindex(t::Table, row)
    output = []
    for col in t.Columns
        push!(output, col[row])
    end

    return output
end

########################################################################
#################### REPL Output #######################################
########################################################################
function head(t::Table)
    
    # Add Column Names
    output = ""
    
    l = rowheader_length(t)
    output *= format("{:$l}", "")
    for col in t.Columns
        output *= "| $(col.header) "
    end
    
    # Add a line-break and a horizontal line
    k = length(output)
    output *= "\n"
    output *= "-"^k
    output *= "\n"
    
    return output

end

function body(t::Table)
    
    # Start the output string
    output = ""
    
    # Get the row-header length
    l = rowheader_length(t)

    for key in t.RowHeader
        row = t[key] 
        
        # Print the name
        output *= format("{:>$l}", String(key) )
        for val in row
            output *= "| $val " 
        end

        # New line
        output *= "\n"
    end

    return output
end

Base.show(io::IO, t::Table) = print(io, head(t)*body(t))
########################################################################
#################### Latex Table Output ################################
########################################################################

function tex_head(t::Table)
    
    # Make Alignment
    align = "r|"
    for col in t.Columns
        align *= "c"
    end

    # Add Column Names
    output = "\\begin{tabular}{$align}\n\\toprule \n"
    
    l = rowheader_length(t)
    output *= format("{:$l}", "")
    for col in t.Columns
        output *= "\& $(col.header) "
    end
    
    # Add a line-break and a horizontal line
    output *= "\\\\ \\hline \n"
    
    return output

end

rowheader_length(t::Table) = maximum(length.(String.(t.RowHeader)))

function tex_body(t::Table)
    
    # Start the output string
    output = ""
    
    # Get the row-header length
    l = rowheader_length(t)

    for key in t.RowHeader
        row = t[key] 
        
        # Print the name
        output *= format("{:>$l}", String(key) )
        for val in row
            output *= "\& $val " 
        end

        # New line
        output *= "\\\\ \n"
    end

    return output
end

tex_foot(t::Table) = "\\bottomrule \n\\end{tabular}"

function tex(t::Table)
    return prod([tex_head(t), tex_body(t), tex_foot(t)])
end

function write_tex(outfile, t::Table)
    open(outfile, "w") do f
        write(f, tex(t))
    end
end

end

using Tables

